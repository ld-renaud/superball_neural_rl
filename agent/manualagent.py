class ManualAgent(object):
    def __init__(self, action_space):
        self.info = {"class": type(self).__name__}

        self.action_space = action_space

    def act(self, observation, reward, done):
        if done:
            return 0
        else:
            while True:
                action = input("Choose an action: ")
                try:
                    action = int(action)
                except ValueError:
                    print("Expected an integer")
                    continue
                if action in self.action_space:
                    return action
                else:
                    print("Specified action is not in the action space:")
                    continue

    def reset_agent(self, seed=None):
        pass
