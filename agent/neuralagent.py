import bisect
import importlib
import numbers
from copy import deepcopy

import numpy as np

import nengo

from .neural_rl import RLmodel


class NeuralAgent:
    def __init__(
        self,
        state_dim,
        action_dim,
        n_neurons=100,
        neuron_type=nengo.LIF(),
        learning_rate=1e-3,
        discount=0.9,
        noiselevel=0.03,
        Q_radius=1,
        reward_radius=1,
        state_dist_thresh=0.3,
        grbf_encoders=False,
        grbf_sigma="default",
        grbf_n_neurons=100,
        img_dim=None,
        gabor_encoders=True,
        gabor_filters_dim=None,
        gabor_n_neurons=100,
        probe=True,
        learning_duration=0.3,
        reset_duration=0.3,
        selection_duration=0.1,
        delay_duration=0.2,
        nengo_dt=0.001,
        seed=None,
        backend="nengo",
        gpu=None,
    ):
        # Saving parameters in serializable dict
        ########################################
        self.info = deepcopy(locals())
        del self.info["self"]
        self.info["class"] = type(self).__name__
        self.info["neuron_type"] = type(neuron_type).__name__

        # Setting backend
        ########################################
        self.backend = backend
        if backend == "nengo":
            backend = nengo
            sim_args = {"optimize": True}
        elif backend == "nengo_dl":
            backend = importlib.import_module(backend)
            sim_args = {"device": "/cpu:0" if gpu is None else "/gpu:{}".format(gpu)}
        elif backend == "nengo_loihi":
            backend = importlib.import_module(backend)
            backend.set_defaults()
            sim_args = {}
        else:
            raise ValueError("Unknown nengo backend")

        # Creating member variables
        ########################################
        self.state_dim = state_dim

        # Timing in model
        self.learning_duration = learning_duration
        self.reset_duration = reset_duration
        self.selection_duration = selection_duration
        self.delay_duration = delay_duration

        self._first_action = True
        self._probe_index = 0

        # Placeholders I/O of the nengo model
        #######################################
        self._state = [0] * state_dim
        self._reward = [0]
        self._reset = [lambda t: False]
        self._inhib_td_error = [lambda t: True]
        self._inhib_current_Q = [lambda t: False]
        self._action = [0] * action_dim

        # Building nengo model
        ######################
        self.model = RLmodel(
            state_signal=self._state,
            reward_signal=self._reward,
            reset_signal=self._reset,
            error_inhib_signal=self._inhib_td_error,
            current_Q_inhib_signal=self._inhib_current_Q,
            action_signal=self._action,
            n_neurons=n_neurons,
            neuron_type=neuron_type,
            learning_rate=learning_rate,
            discount=discount,
            noiselevel=noiselevel,
            Q_radius=Q_radius,
            reward_radius=reward_radius,
            state_dist_thresh=state_dist_thresh,
            grbf_encoders=grbf_encoders,
            grbf_sigma=grbf_sigma,
            grbf_n_neurons=grbf_n_neurons,
            img_dim=img_dim,
            gabor_encoders=gabor_encoders,
            gabor_filters_dim=gabor_filters_dim,
            gabor_n_neurons=gabor_n_neurons,
            probe=probe,
        )

        self.sim = backend.Simulator(
            network=self.model, seed=seed, dt=nengo_dt, **sim_args
        )

        return

    def act(self, state, reward, done):
        # Update state and reward
        #########################
        self.state = state
        self.reward = reward

        if not done:
            if self._first_action:
                self._first_action = False

                # 1st step: delay before reset
                ######################################
                t1 = 0
                # 2nd step: reset network
                #######################################
                t2 = t1 + self.delay_duration
                run_duration = t2 + self.reset_duration

                self.reset = {t1: False, t2: True}
                self.inhib_td_error = True
                self.inhib_current_Q = False

            else:
                # 1st step: delay before learning
                ######################################
                t1 = 0
                # 2nd step: learn from reward observed
                #######################################
                t2 = t1 + self.delay_duration
                # 3rd step: selection of action
                #######################################
                t3 = t2 + self.learning_duration
                # 4th step: reset network
                #######################################
                t4 = t3 + self.selection_duration
                run_duration = t4 + self.reset_duration

                self.reset = {t1: False, t4: True}
                self.inhib_td_error = {t1: True, t2: False, t3: True}
                self.inhib_current_Q = False
            ### end if _first_action

            self.sim.run(run_duration)
            chosen_action = int(np.argmax(self._action))
            return chosen_action
        else:
            # 1st step: delay before learning
            ######################################
            t1 = 0
            # 2nd step: learn from reward observed
            #######################################
            t2 = t1 + self.delay_duration
            run_duration = t2 + self.learning_duration

            self.reset = False
            self.inhib_td_error = {t1: True, t2: False}
            self.inhib_current_Q = True

            self.sim.run(run_duration)
            self._first_action = True
            return
        ### end if not done

    def test(self):
        self.reset = {0: 1, 1: 0}
        self.sim.run(2)

    def get_probe_data(self):
        # Get probe data
        probe_data = dict()
        for probe in self.model.all_probes:
            probe_data[probe.label] = self.sim.data[probe][self._probe_index :].tolist()

        # Get matching data
        trange = self.sim.trange(sample_every=probe.sample_every)[
            -len(probe_data[probe.label]) :
        ].tolist()
        self._probe_index += len(probe_data[probe.label])

        return trange, probe_data

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, new_state):
        for i in range(len(self._state)):
            self._state[i] = new_state[i]

    @property
    def reward(self):
        return self._reward

    @reward.setter
    def reward(self, new_reward):
        self._reward[0] = new_reward

    @property
    def inhib_td_error(self):
        return self._inhib_td_error

    @inhib_td_error.setter
    def inhib_td_error(self, new_inhib_td_error):
        self._inhib_td_error[0] = self._piecewise_steps_function(new_inhib_td_error)

    @property
    def inhib_current_Q(self):
        return self._inhib_current_Q

    @inhib_current_Q.setter
    def inhib_current_Q(self, new_inhib_current_Q):
        self._inhib_current_Q[0] = self._piecewise_steps_function(new_inhib_current_Q)

    @property
    def reset(self):
        return self._reset

    @reset.setter
    def reset(self, new_reset):
        self._reset[0] = self._piecewise_steps_function(new_reset)

    def _piecewise_steps_function(self, data):
        if isinstance(data, numbers.Number):

            def function(t):
                return data

        elif isinstance(data, dict):
            if 0 not in data:
                data[0] = 0

            new_data = {}
            for key in data:
                new_data[key + self.sim.time] = data[key]

            step_times = sorted(new_data.keys())

            def function(t):
                i = bisect.bisect(step_times, t)
                return new_data[step_times[i - 1]]

        return function

    def reset_agent(self, seed=None):
        self._first_action = True
        self._probe_index = 0

        self.sim.reset(seed)
