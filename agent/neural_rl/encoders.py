import numpy as np

import nengo
from nengo_extras.vision import Gabor, Mask
from sobol_seq import i4_sobol_generate


def GrbfEncoder(
    n_neurons,
    input_dim,
    input_boundaries,
    sigma="default",
    points=None,
    seed=None,
    **kwargs,
):
    input_boundaries = np.sort(np.array(input_boundaries), axis=1)
    if input_boundaries.shape[0] != input_dim:
        raise ValueError("Not enough boundaries per dimension")
    elif input_boundaries.shape[1] != 2:
        raise ValueError("More than 2 boundaries per dimension")

    sigma = (
        sigma
        if sigma != "default"
        else np.mean(np.absolute(input_boundaries[:, 1] - input_boundaries[:, 0]))
        / np.sqrt(n_neurons)
    )
    # print("sqrt:{}".format(np.sqrt(n_neurons)))
    # print("sigma:{}".format(sigma))

    if points is None:
        if seed is not None:
            np.random.seed(seed)

        # Temporary code - Replace with more general implementation
        ############################################################
        if input_dim == 3:
            n_neurons_pos = int(np.ceil(n_neurons / 2))
            n_neurons_neg = int(np.floor(n_neurons / 2))

            P_pos = np.array(i4_sobol_generate(2, n_neurons_pos)) * 2 - 1
            P_neg = np.array(i4_sobol_generate(2, n_neurons_neg)) * 2 - 1
            P = np.vstack((P_pos, P_neg))

            P_binary = np.array([1] * n_neurons_pos + [-1] * n_neurons_neg).reshape(
                n_neurons, 1
            )

            P = np.hstack((P, P_binary))
        else:
            P = np.array(i4_sobol_generate(input_dim, n_neurons)) * 2 - 1
            # P = P[P[:, 0].argsort()]
            # P = list(P[P[:, 1].argsort()])
        ############################################################

    else:
        P = np.array(points)
        if P.shape[0] != n_neurons and P.shape[1] != input_dim:
            raise ValueError("Provided points does not match dimensions")

    def encode(t, x):
        return np.array(
            [np.exp(-np.linalg.norm(x - p) ** 2 / (2 * sigma ** 2)) for p in P]
        )

    with nengo.Network(**kwargs) as encoder:
        encoder.P = P
        encoder.input = nengo.Node(size_in=input_dim, size_out=n_neurons, output=encode)
        encoder.output = nengo.Ensemble(
            n_neurons,
            dimensions=1,
            encoders=nengo.dists.Choice(options=np.array([[1.0]])),
            intercepts=nengo.dists.Choice(options=np.array([0])),
            max_rates=nengo.dists.Choice(options=np.array([400])),
            label="output",
        )
        for i in range(n_neurons):
            nengo.Connection(encoder.input[i], encoder.output.neurons[i])
    return encoder


def GaborGrbfEncoder(
    grbf_dim,
    img_dim,
    n_neurons_grbf,
    n_neurons_img,
    grbf_points=None,
    grbf_sigma="default",
    gabor_filters_dim=None,
    gabor_encoders=None,
    seed=None,
    **kwargs,
):
    transform_factor = 5e-4
    with nengo.Network(**kwargs) as state_encoder:
        state_encoder.output = nengo.Ensemble(
            n_neurons=n_neurons_grbf + n_neurons_img,
            dimensions=1,
            encoders=nengo.dists.Choice(options=np.array([[1.0]])),
            intercepts=nengo.dists.Choice(options=np.array([0])),
            max_rates=nengo.dists.Choice(options=np.array([400])),
            label="output",
        )

        state_encoder.grbf_encoder = GrbfEncoder(
            n_neurons=n_neurons_grbf,
            input_dim=grbf_dim,
            input_boundaries=np.array([[-1, 1]] * grbf_dim),
            sigma=grbf_sigma,
            points=grbf_points,
            label="grbf_encoder",
        )
        state_encoder.grbf_input = state_encoder.grbf_encoder.input

        for i in range(n_neurons_grbf):
            nengo.Connection(
                state_encoder.grbf_encoder.output.neurons[i],
                state_encoder.output.neurons[i],
                transform=transform_factor,
            )

        gabor_filter_n = n_neurons_img if n_neurons_img > 0 else 1
        img_rows, img_cols = img_dim

        if gabor_encoders is None:
            if gabor_filters_dim is None:
                gabor_filters_dim = img_dim
            gabor_filters = Gabor().generate(gabor_filter_n, gabor_filters_dim)
            gabor_encoders = Mask((img_rows, img_cols)).populate(
                gabor_filters, flatten=True
            )
        state_encoder.gabor_encoders = gabor_encoders

        state_encoder.img_input = nengo.Ensemble(
            n_neurons=gabor_filter_n,
            dimensions=img_rows * img_cols,
            intercepts=nengo.dists.Choice([0]),
            max_rates=nengo.dists.Choice([150]),
            label="img_input",
        )
        state_encoder.img_input.encoders = gabor_encoders

        for i in range(n_neurons_img):
            nengo.Connection(
                state_encoder.img_input.neurons[i],
                state_encoder.output.neurons[i + n_neurons_grbf],
                transform=transform_factor,
            )
    return state_encoder


def PixelGrbfEncoder(
    grbf_dim,
    img_dim,
    n_neurons_grbf,
    grbf_points=None,
    grbf_sigma="default",
    seed=None,
    **kwargs,
):
    transform_factor = 5e-4
    img_rows, img_cols = img_dim
    n_neurons_img = img_rows * img_cols

    with nengo.Network(**kwargs) as state_encoder:
        state_encoder.output = nengo.Ensemble(
            n_neurons=n_neurons_grbf + n_neurons_img,
            dimensions=1,
            encoders=nengo.dists.Choice(options=np.array([[1.0]])),
            intercepts=nengo.dists.Choice(options=np.array([0])),
            max_rates=nengo.dists.Choice(options=np.array([400])),
            label="output",
        )

        state_encoder.grbf_encoder = GrbfEncoder(
            n_neurons=n_neurons_grbf,
            input_dim=grbf_dim,
            input_boundaries=np.array([[-1, 1]] * grbf_dim),
            sigma=grbf_sigma,
            points=grbf_points,
            label="grbf_encoder",
        )
        state_encoder.grbf_input = state_encoder.grbf_encoder.input

        for i in range(n_neurons_grbf):
            nengo.Connection(
                state_encoder.grbf_encoder.output.neurons[i],
                state_encoder.output.neurons[i],
                transform=transform_factor,
            )

        state_encoder.img_input = nengo.Node(size_in=n_neurons_img)

        for i in range(n_neurons_img):
            nengo.Connection(
                state_encoder.img_input[i],
                state_encoder.output.neurons[i + n_neurons_grbf],
                transform=transform_factor,
            )
    return state_encoder
