import numpy as np

import nengo

from .utils import Inhibition, Integrator


def ErrorCalculation(
    action_dim,
    n_neurons=100,
    neuron_type=nengo.LIF(),
    discount=0.9,
    positive_bias=0.03,
    probe=False,
    label="error_calculation",
    **kwargs
):
    with nengo.Network(label=label, **kwargs) as error_calculation:
        # Inputs
        error_calculation.prev_Q = nengo.Node(size_in=action_dim)
        error_calculation.current_Q = nengo.Node(size_in=action_dim)
        error_calculation.prev_selection = nengo.Node(size_in=action_dim)
        error_calculation.current_selection = nengo.Node(size_in=action_dim)
        error_calculation.reward_input = nengo.Node(size_in=1)
        error_calculation.reset = nengo.Node(size_in=1)
        error_calculation.inhib_learn = nengo.Node(size_in=1)
        error_calculation.inhib_current_Q = nengo.Node(size_in=1)

        # Outputs
        error_calculation.output = nengo.Node(size_in=action_dim)

        # Internals
        prev_Q_selection = Selection(
            action_dim,
            n_neurons=n_neurons,
            neuron_type=neuron_type,
            label="prev_Q_selection",
        )
        nengo.Connection(error_calculation.prev_Q, prev_Q_selection.Q_input)
        nengo.Connection(
            error_calculation.prev_selection, prev_Q_selection.selection_input
        )

        current_Q_selection = Selection(
            action_dim,
            n_neurons=n_neurons,
            neuron_type=neuron_type,
            label="current_Q_selection",
        )
        nengo.Connection(error_calculation.current_Q, current_Q_selection.Q_input)
        nengo.Connection(
            error_calculation.current_selection, current_Q_selection.selection_input
        )
        Inhibition(error_calculation.inhib_current_Q, current_Q_selection.output)

        # integrative_discount = Integrator(
        #     dimensions=1,
        #     recurrent_tau=0.1,
        #     feedback=discount,
        #     label="integrative_discount",
        # )
        # nengo.Connection(prev_Q_selection.output, integrative_discount.input)
        # nengo.Connection(error_calculation.reset, integrative_discount.reset)

        # reward_integration = Integrator(
        #     dimensions=1, recurrent_tau=0.1, label="reward_integration"
        # )
        # nengo.Connection(error_calculation.reward_input, reward_integration.input)
        # nengo.Connection(error_calculation.reset, reward_integration.reset)

        error = nengo.Ensemble(
            n_neurons=n_neurons, dimensions=1, neuron_type=neuron_type, label="error"
        )
        nengo.Connection(
            error_calculation.reward_input, error
        )  # nengo.Connection(reward_integration.ensemble, error)
        nengo.Connection(current_Q_selection.output, error, transform=discount)
        nengo.Connection(prev_Q_selection.output, error, transform=-1)
        # nengo.Connection(integrative_discount.ensemble, error, transform=-1)

        error_vector = nengo.networks.EnsembleArray(
            n_neurons, action_dim, label="error_vector"
        )
        one_node = nengo.Node(1, label="inhibition")
        for i, ensemble in enumerate(error_vector.ea_ensembles):
            nengo.Connection(error, ensemble)
            inib_strength = 2.5
            Inhibition(one_node, ensemble, strenght=inib_strength)
            Inhibition(
                error_calculation.prev_selection[i], ensemble, strenght=-inib_strength
            )
            Inhibition(error_calculation.inhib_learn, ensemble)

        nengo.Connection(error_vector.output, error_calculation.output)

        # posbias = PositiveBias(
        #     action_dim, n_neurons=n_neurons, neuron_type=neuron_type, bias=positive_bias
        # )
        # nengo.Connection(error_calculation.prev_Q, posbias.input)
        # nengo.Connection(error_calculation.inhib_learn, posbias.inib)
        # nengo.Connection(posbias.output, error_calculation.output)

        if probe:
            probes = [
                nengo.Probe(
                    error_calculation.reward_input,
                    label="{}.reward_input".format(label),
                ),
                nengo.Probe(
                    prev_Q_selection.output, label="{}.prev_Q_selection".format(label)
                ),
                nengo.Probe(
                    current_Q_selection.output,
                    label="{}.current_Q_selection".format(label),
                ),
                nengo.Probe(error, label="{}.error".format(label)),
                nengo.Probe(
                    error_calculation.output, label="{}.error_vector".format(label)
                ),
                # nengo.Probe(
                #     integrative_discount.ensemble,
                #     attr="decoded_output",
                #     label="{}.integrative_discount".format(label),
                # ),
                # nengo.Probe(posbias.output, label="{}.posbias".format(label)),
            ]

    return error_calculation


def Selection(action_dim, n_neurons=100, neuron_type=nengo.LIF(), **kwargs):
    with nengo.Network(**kwargs) as selection:
        selection.Q_input = nengo.Node(size_in=action_dim)
        selection.selection_input = nengo.Node(
            size_in=action_dim, output=lambda t, x: np.ones(action_dim) - x
        )
        Q_values = nengo.networks.EnsembleArray(
            n_neurons=n_neurons, n_ensembles=action_dim, label="values"
        )
        selection.output = nengo.Ensemble(
            n_neurons=n_neurons,
            dimensions=1,
            neuron_type=neuron_type,
            label="selected_value",
        )
        nengo.Connection(selection.Q_input, Q_values.input)

        for i, ensemble in enumerate(Q_values.ea_ensembles):
            nengo.Connection(ensemble, selection.output)
            inib_strength = 2.5
            Inhibition(selection.selection_input[i], ensemble, strenght=inib_strength)

    return selection


def PositiveBias(
    dimensions,
    n_neurons=100,
    neuron_type=nengo.LIF(),
    bias=0.03,
    label="positive_bias",
    **kwargs
):
    with nengo.Network(label, **kwargs) as posbias:
        posbias.input = nengo.Ensemble(
            n_neurons=n_neurons, dimensions=dimensions, neuron_type=neuron_type
        )
        posbias.inib = nengo.Node(size_in=1)
        posbias.output = nengo.Node(size_in=dimensions)

        neg_thresh = nengo.networks.EnsembleArray(
            n_neurons=int(n_neurons / 3),
            n_ensembles=dimensions,
            label="negative threshold",
        )
        for i, ensemble in enumerate(neg_thresh.ea_ensembles):
            thresh_value = 0
            ensemble.encoders = nengo.dists.Choice(options=np.array([[1.0]]))
            ensemble.eval_points = nengo.dists.Uniform(thresh_value, ensemble.radius)
            ensemble.intercepts = nengo.dists.Exponential(
                0.15, thresh_value, high=ensemble.radius
            )
            nengo.Connection(posbias.input[i], ensemble, function=lambda x: x > 0)

        bias_node = nengo.Node(bias, label="bias")
        bias_vector = nengo.networks.EnsembleArray(
            n_neurons=n_neurons, n_ensembles=dimensions, label="bias vector"
        )
        for i, ensemble in enumerate(bias_vector.ea_ensembles):
            nengo.Connection(bias_node, ensemble)
            Inhibition(neg_thresh.ea_ensembles[i], ensemble)
            Inhibition(posbias.inib, ensemble)
        nengo.Connection(bias_vector.output, posbias.output)
    return posbias
