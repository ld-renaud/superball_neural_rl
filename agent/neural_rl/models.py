import nengo

from .action_selection import ActionSelection
from .action_values import ActionValues
from .image_action_values import ImageActionValues
from .error_calculation import ErrorCalculation


def RLmodel(
    state_signal,
    reward_signal,
    reset_signal,
    error_inhib_signal,
    current_Q_inhib_signal,
    action_signal,
    n_neurons=100,
    neuron_type=nengo.LIF(),
    learning_rate=1e-3,
    discount=0.9,
    noiselevel=0.03,
    Q_radius=1,
    reward_radius=1,
    state_dist_thresh=0.3,
    grbf_encoders=False,
    grbf_sigma="default",
    grbf_n_neurons=100,
    img_dim=None,
    gabor_encoders=True,
    gabor_filters_dim=None,
    gabor_n_neurons=100,
    label="nrl_model",
    probe=False,
    **kwargs
):
    with nengo.Network(label, **kwargs) as net:
        net.config[nengo.Connection].synapse = .007
        net.config[nengo.Ensemble].neuron_type = neuron_type
        net.config[nengo.Probe].sample_every = 0.02  # seconds
        net.config[nengo.Probe].synapse = 0.01

        state_dim = len(state_signal)
        action_dim = len(action_signal)

        def update_action_signal(t, x):
            for i in range(action_dim):
                action_signal[i] = x[i]
            return x

        net.state = nengo.Node(lambda t: state_signal)
        net.inhib_td_error = nengo.Node(lambda t: error_inhib_signal[0](t))
        net.reset = nengo.Node(lambda t: reset_signal[0](t))
        net.reward = nengo.Node(lambda t: reward_signal)
        net.inhib_current_Q = nengo.Node(lambda t: current_Q_inhib_signal[0](t))
        net.action = nengo.Node(output=update_action_signal, size_in=action_dim)

        if img_dim is None:
            action_values = ActionValues(
                state_dim,
                action_dim,
                n_neurons,
                neuron_type,
                learning_rate,
                Q_radius,
                state_dist_thresh,
                grbf_encoders=grbf_encoders,
                grbf_sigma=grbf_sigma,
                grbf_n_neurons=grbf_n_neurons,
                probe=probe,
            )
        else:
            flatten_img_dim = img_dim[0] * img_dim[1]
            grbf_dim = state_dim - flatten_img_dim
            action_values = ImageActionValues(
                grbf_dim,
                img_dim,
                action_dim,
                n_neurons,
                neuron_type,
                learning_rate,
                Q_radius,
                state_dist_thresh,
                grbf_sigma=grbf_sigma,
                grbf_n_neurons=grbf_n_neurons,
                gabor_encoders=gabor_encoders,
                gabor_filters_dim=gabor_filters_dim,
                gabor_n_neurons=gabor_n_neurons,
                probe=probe,
            )

        action_selection = ActionSelection(
            action_dim,
            n_neurons,
            neuron_type,
            noiselevel,
            scaling=1 / (2 * Q_radius),
            probe=probe,
        )

        error_calculation = ErrorCalculation(
            action_dim, n_neurons, neuron_type, discount, probe=probe
        )

        nengo.Connection(action_values.current_Q, action_selection.input)
        nengo.Connection(action_values.current_Q, error_calculation.current_Q)
        nengo.Connection(action_values.prev_Q, error_calculation.prev_Q)
        nengo.Connection(action_selection.prev_output, error_calculation.prev_selection)
        nengo.Connection(action_selection.output, error_calculation.current_selection)
        nengo.Connection(error_calculation.output, action_values.td_error)
        nengo.Connection(net.inhib_current_Q, error_calculation.inhib_current_Q)
        nengo.Connection(net.state, action_values.state_input)
        nengo.Connection(net.inhib_td_error, error_calculation.inhib_learn)
        nengo.Connection(action_selection.prev_output, net.action)
        nengo.Connection(net.reset, error_calculation.reset)
        nengo.Connection(net.reset, action_values.memory_gate)
        nengo.Connection(net.reset, action_selection.memory_gate)
        nengo.Connection(net.reset, action_selection.noise_node)
        nengo.Connection(net.reward, error_calculation.reward_input)

        # if probe:
        #     probes = [
        #         nengo.Probe(net.state, label="{}.state".format(label)),
        #         nengo.Probe(net.action, label="{}.action".format(label)),
        #         nengo.Probe(
        #             net.inhib_td_error, label="{}.inhib_td_error".format(label)
        #         ),
        #         nengo.Probe(
        #             net.reset, label="{}.reset".format(label)
        #         ),
        #     ]

    print("RL model built\nNumber of neurons:{}".format(net.n_neurons))
    print(net.config)

    return net
