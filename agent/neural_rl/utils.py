import numpy as np

import nengo


def Inhibition(inhibitor, inhibited, strenght=2.5):
    return nengo.Connection(
        inhibitor, inhibited.neurons, transform=[[-strenght]] * inhibited.n_neurons
    )


def Integrator(
    dimensions,
    n_neurons=100,
    neuron_type=nengo.LIF(),
    recurrent_tau=0.1,
    feedback=1,
    **kwargs
):
    with nengo.Network(**kwargs) as integrator:
        integrator.input = nengo.Node(size_in=dimensions)
        integrator.reset = nengo.Node(size_in=dimensions)
        integrator.ensemble = nengo.Ensemble(
            n_neurons=n_neurons, dimensions=dimensions, neuron_type=neuron_type
        )

        nengo.Connection(integrator.input, integrator.ensemble, transform=recurrent_tau)
        nengo.Connection(
            integrator.ensemble,
            integrator.ensemble,
            transform=feedback,
            synapse=recurrent_tau,
        )
        Inhibition(integrator.reset, integrator.ensemble)

    return integrator


def Inversor(dimensions, n_neurons=100, neuron_type=nengo.LIF(), **kwargs):
    with nengo.Network(**kwargs) as inversor:
        inversor.ensemble = nengo.Ensemble(
            n_neurons=n_neurons, dimensions=dimensions, neuron_type=neuron_type
        )
        inversor.input = nengo.node(size_in=dimensions)
        one_input = nengo.Node([1] * dimensions)
        nengo.Connection(inversor.input, inversor.ensemble, transform=-1)
        nengo.Connection(one_input, inversor.ensemble)
    return inversor


def Distance(n_neurons, dimensions, neuron_type=nengo.LIF(), **kwargs):
    with nengo.Network(**kwargs) as distance_calculator:
        distance_calculator.input1 = nengo.Node(size_in=dimensions)
        distance_calculator.input2 = nengo.Node(size_in=dimensions)
        distance_calculator.output = nengo.Node(size_in=1, label="output")

        difference = nengo.Ensemble(
            n_neurons=n_neurons,
            dimensions=dimensions,
            neuron_type=neuron_type,
            label="vector difference",
        )

        distance_calculator.norm = nengo.Ensemble(
            n_neurons=n_neurons, dimensions=1, neuron_type=neuron_type, label="norm"
        )

        nengo.Connection(distance_calculator.input1, difference, transform=1)
        nengo.Connection(distance_calculator.input2, difference, transform=-1)
        nengo.Connection(
            difference, distance_calculator.norm, function=lambda x: np.linalg.norm(x)
        )
        nengo.Connection(distance_calculator.norm, distance_calculator.output)

    return distance_calculator
