import numpy as np

import nengo


def ActionSelection(
    action_dim,
    n_neurons=100,
    neuron_type=nengo.LIF(),
    noiselevel=0.03,
    shift=0.5,
    scaling=1,
    probe=False,
    label="action_selection",
    **kwargs
):
    with nengo.Network(label, **kwargs) as action_selection:
        # Inputs
        action_selection.input = nengo.Node(size_in=action_dim)
        action_selection.memory_gate = nengo.Node(size_in=1)
        action_selection.noise_node = NoiseNode(
            size_out=action_dim, noiselevel=noiselevel, pos_edge=False
        )

        #  Outputs
        action_selection.output = nengo.Node(size_in=action_dim)
        action_selection.prev_output = nengo.Node(size_in=action_dim)

        # Internals
        nengo.Connection(action_selection.noise_node, action_selection.input)

        bg = nengo.networks.BasalGanglia(
            dimensions=action_dim, n_neurons_per_ensemble=n_neurons, input_bias=shift
        )
        nengo.Connection(action_selection.input, bg.input, transform=scaling)

        thal = nengo.networks.Thalamus(
            dimensions=action_dim, n_neurons_per_ensemble=n_neurons, mutual_inhib=1.05
        )
        nengo.Connection(bg.output, thal.input)

        threshold = 0.05
        thresholding = nengo.networks.EnsembleArray(
            n_neurons=int(n_neurons / 3), n_ensembles=action_dim, label="threshold"
        )
        for i, ensemble in enumerate(thresholding.ea_ensembles):
            ensemble.encoders = nengo.dists.Choice(options=np.array([[1.0]]))
            ensemble.eval_points = nengo.dists.Uniform(threshold, ensemble.radius)
            ensemble.intercepts = nengo.dists.Exponential(
                0.15, threshold, high=ensemble.radius
            )
            nengo.Connection(
                thal.actions.ea_ensembles[i], ensemble, function=lambda x: x > threshold
            )
        nengo.Connection(thresholding.output, action_selection.output)

        saved_max = nengo.networks.InputGatedMemory(
            n_neurons=n_neurons,
            dimensions=action_dim,
            difference_gain=5,
            label="saved_max",
        )
        nengo.Connection(thresholding.output, saved_max.input)
        nengo.Connection(saved_max.output, action_selection.prev_output)
        nengo.Connection(nengo.Node(output=1, label="one input"), saved_max.gate)
        nengo.Connection(action_selection.memory_gate, saved_max.gate, transform=-1)

        if probe:
            probes = [
                nengo.Probe(action_selection.input, label="{}.input".format(label)),
                nengo.Probe(
                    action_selection.noise_node, label="{}.noise_node".format(label)
                ),
                nengo.Probe(saved_max.output, label="{}.saved_max".format(label)),
                # nengo.Probe(bg.output, label="{}.basal_ganglia".format(label)),
                # nengo.Probe(bg.input, label="{}.rescaled_input".format(label)),
                # nengo.Probe(thal.output, label="{}.thalamus".format(label)),
                nengo.Probe(action_selection.output, label="{}.output".format(label)),
            ]

    return action_selection


class NoiseNode(nengo.Node):
    def __init__(
        self, size_out, noiselevel, pos_edge=True, threshold=0.5, label="noise_node"
    ):
        super().__init__(size_in=1, label=label)
        self.prev_input = 0
        self.threshold = threshold
        self.size_out = size_out
        self.noiselevel = noiselevel
        self.current_output = np.random.normal(scale=noiselevel, size=self.size_out)
        self.output = self._output_func

        if pos_edge:

            def _edge(input):
                return self.prev_input < self.threshold and input > self.threshold

        else:

            def _edge(input):
                return self.prev_input > self.threshold and input < self.threshold

        self._edge = _edge

    def _output_func(self, t, input):
        if self._edge(input):
            self.current_output = np.random.normal(
                scale=self.noiselevel, size=self.size_out
            )
        self.prev_input = input
        return self.current_output
