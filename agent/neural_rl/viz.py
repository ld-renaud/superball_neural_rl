import nengo
import sys
from pathlib import Path

sys.path.append(Path(__file__).parents[1].resolve().as_posix())

encoders = True

state_dim = 3
img_dim = (5, 5)


state = [0] * (3 + img_dim[0] * img_dim[1])
action = [0] * 2
reset = [lambda t: 0]
inhib_learn = [lambda t: 1]
inhib_current_Q = [lambda t: 0]
reward = [0]

with nengo.Network() as model:
    from neural_rl.models import RLmodel

    RLmodel(
        state,
        reward,
        reset,
        inhib_learn,
        inhib_current_Q,
        action,
        n_neurons=200,
        grbf_encoders=encoders,
        img_dim=img_dim
    )
