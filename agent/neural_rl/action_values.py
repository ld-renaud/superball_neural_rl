import numpy as np

import nengo

from .encoders import GrbfEncoder
from .utils import Inhibition, Distance


def ActionValues(
    state_dim,
    action_dim,
    n_neurons=100,
    neuron_type=nengo.LIF(),
    learning_rate=1e-3,
    Q_radius=1,
    state_dist_thresh=0.3,
    grbf_encoders=False,
    grbf_sigma="default",
    grbf_n_neurons=100,
    Q_weights=None,
    label="action_values",
    probe=False,
    **kwargs
):
    with nengo.Network(label, **kwargs) as action_values:
        # Inputs
        ##########
        action_values.state_input = nengo.Node(size_in=state_dim)
        action_values.td_error = nengo.Node(size_in=action_dim)
        action_values.memory_gate = nengo.Node(size_in=1)

        # Outputs
        ##########
        action_values.prev_Q = nengo.Node(size_in=action_dim)
        action_values.current_Q = nengo.Node(size_in=action_dim)

        # Internals
        ############
        # current and previous state representations
        state = nengo.Ensemble(
            n_neurons=n_neurons * state_dim,
            dimensions=state_dim,
            neuron_type=neuron_type,
            label="state",
        )
        nengo.Connection(action_values.state_input, state)

        prev_state = nengo.Ensemble(
            n_neurons=n_neurons * state_dim,
            dimensions=state_dim,
            neuron_type=neuron_type,
            label="prev_state",
        )

        # Encoding of state
        if grbf_encoders:
            encoded_state_dim = grbf_n_neurons
            state_encoder = GrbfEncoder(
                n_neurons=encoded_state_dim,
                input_dim=state_dim,
                input_boundaries=[[-1, 1]] * state_dim,
                sigma=grbf_sigma,
                label="state_encoder",
            )
            nengo.Connection(state, state_encoder.input)

            prev_state_encoder = GrbfEncoder(
                n_neurons=encoded_state_dim,
                input_dim=state_dim,
                input_boundaries=[[-1, 1]] * state_dim,
                sigma=grbf_sigma,
                points=state_encoder.P,
                label="prev_state_encoder",
            )
            if not np.array_equal(state_encoder.P, prev_state_encoder.P):
                raise ValueError("GRBF encoders are not similar")

            nengo.Connection(prev_state, prev_state_encoder.input)
            encoded_state = state_encoder.output
            encoded_prev_state = prev_state_encoder.output
        else:
            encoded_state_dim = state_dim
            encoded_state = state
            encoded_prev_state = prev_state

        # Memory hodling previous state
        state_memory = nengo.networks.InputGatedMemory(
            n_neurons, dimensions=state_dim, difference_gain=5, label="state_memory"
        )
        nengo.Connection(state_memory.output, prev_state)
        nengo.Connection(state, state_memory.input)
        nengo.Connection(nengo.Node(output=1, label="one_input"), state_memory.gate)
        nengo.Connection(action_values.memory_gate, state_memory.gate, transform=-1)

        # previous Q function
        prev_Q = nengo.Ensemble(
            n_neurons=n_neurons * 3,
            dimensions=action_dim,
            neuron_type=neuron_type,
            radius=Q_radius,
            label="prev_Q",
        )

        if Q_weights is not None:
            prev_Q_func_args = {
                "solver": nengo.solvers.NoSolver(values=Q_weights[0], weights=True)
            }
        else:
            prev_Q_func_args = {
                "function": lambda x: [0.2] * action_dim,
                # "function": lambda x: np.random.random(action_dim),
                "solver": nengo.solvers.LstsqL2(weights=True),
            }

        prev_Q_function = nengo.Connection(
            encoded_prev_state, prev_Q, **prev_Q_func_args
        )
        prev_Q_function.learning_rule_type = {
            "pes": nengo.PES(learning_rate=learning_rate)
        }
        nengo.Connection(
            action_values.td_error, prev_Q_function.learning_rule["pes"], transform=-1
        )
        nengo.Connection(prev_Q, action_values.prev_Q)

        # current Q function
        current_Q = nengo.Ensemble(
            n_neurons=n_neurons * 3,
            dimensions=action_dim,
            neuron_type=neuron_type,
            radius=Q_radius,
            label="current_Q",
        )

        if Q_weights is not None:
            curr_Q_func_args = {
                "solver": nengo.solvers.NoSolver(values=Q_weights[1], weights=True)
            }
        else:
            curr_Q_func_args = {
                "function": lambda x: [0.2] * action_dim,
                # "function": lambda x: np.random.random(action_dim),
                "solver": nengo.solvers.LstsqL2(weights=True),
            }

        current_Q_function = nengo.Connection(
            encoded_state, current_Q, **curr_Q_func_args
        )
        nengo.Connection(current_Q, action_values.current_Q)

        # Difference between the current and previous Q function
        Q_difference = nengo.Ensemble(
            n_neurons=n_neurons,
            dimensions=action_dim,
            neuron_type=neuron_type,
            label="Q_difference",
        )
        nengo.Connection(current_Q, Q_difference, transform=1)
        nengo.Connection(prev_Q, Q_difference, transform=-1)

        # Detection of same previous and current state
        state_distance = Distance(n_neurons, state_dim, label="state_distance")
        nengo.Connection(state, state_distance.input1)
        nengo.Connection(prev_state, state_distance.input2)

        threshold = nengo.Ensemble(
            n_neurons=n_neurons,
            dimensions=1,
            neuron_type=neuron_type,
            encoders=nengo.dists.Choice(options=np.array([[1.0]])),
            eval_points=nengo.dists.Uniform(state_dist_thresh, 1),
            intercepts=nengo.dists.Exponential(0.15, state_dist_thresh, high=1),
            label="state_distance_threshold",
        )
        nengo.Connection(
            state_distance.norm, threshold, function=lambda x: x > state_dist_thresh
        )
        Inhibition(threshold, Q_difference, strenght=10)

        current_Q_function.learning_rule_type = {
            "pes": nengo.PES(learning_rate=learning_rate)
        }
        nengo.Connection(Q_difference, current_Q_function.learning_rule["pes"])

        # Probes
        #########
        if probe:
            probes = [
                nengo.Probe(state, label="{}.state".format(label)),
                nengo.Probe(prev_state, label="{}.prev_state".format(label)),
                nengo.Probe(prev_Q, label="{}.prev_Q".format(label)),
                nengo.Probe(current_Q, label="{}.current_Q".format(label)),
                nengo.Probe(Q_difference, label="{}.Q_difference".format(label)),
                nengo.Probe(
                    state_distance.output, label="{}.state_distance".format(label)
                ),
                # nengo.Probe(
                #     threshold, label="{}.state_distance_threshold".format(label)
                # ),
            ]

    return action_values
