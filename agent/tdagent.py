from copy import deepcopy

import numpy as np

from sobol_seq import i4_sobol_generate


def softmax(xs):
    exp_vals = np.exp(xs)
    return exp_vals / np.sum(exp_vals)


class TDAgent(object):
    def __init__(
        self,
        action_space,
        input_dim=2,
        n_neurons=100,
        discount=0.9,
        learning_rate=1e-3,
        epsilon=0.01,
        sigma=0.2,
    ):
        # Saving parameters in serializable dict
        ########################################
        self.info = deepcopy(locals())
        del self.info["self"]
        del self.info["action_space"]
        self.info["class"] = type(self).__name__

        self.epsilon = epsilon
        self.action_space = action_space
        self.n_neurons = n_neurons
        self.discount = discount
        self.step_size = learning_rate

        self.w = np.zeros((self.action_space.n, self.n_neurons + 1))
        self.sigma = sigma
        # Temporary code - Replace with more general implementation
        ############################################################
        if input_dim == 3:
            P = np.array(i4_sobol_generate(2, n_neurons)) * 2 - 1
            P_binary = np.random.choice([-1, 1], n_neurons).reshape(n_neurons, 1)
            P = np.hstack((P, P_binary))
        else:
            P = np.array(i4_sobol_generate(input_dim, n_neurons)) * 2 - 1
        self.P = P
        ############################################################
        self.agent_start()

    def obs_to_vec(self, obs):
        obs_vec = np.array(
            [
                np.exp(-np.linalg.norm(obs - p) ** 2 / (2 * self.sigma ** 2))
                for p in self.P
            ]
        )
        print(np.where(obs_vec > 0.1))
        return np.hstack((obs_vec, np.array([1])))

    def agent_start(self):
        self.last_state = None
        self.last_action = None
        self.first_action = True

    def get_action(self, state):

        avals = np.dot(self.w, state)
        print("Action values: ", avals)
        retval = int(np.random.choice(np.where(avals == np.max(avals))[0]))
        if np.random.random() < self.epsilon:
            retval = int(np.random.choice(range(len(avals))))
        ### end if
        return retval

    #         pvals = softmax(np.dot(self.w, state))
    #         return int(np.argmax(np.random.multinomial(1,pvals)))

    def act(self, observation, reward, done):
        obs_vec = self.obs_to_vec(observation)
        #         print('State: ', obs_vec)
        action = self.get_action(obs_vec)

        if not self.first_action:
            target = reward - self.q(self.last_state, self.last_action)
            if not done:
                target += self.discount * self.q(obs_vec, action)
            ### end if
            dw = self.dq(self.last_state, self.last_action)
            self.w += self.step_size * target * dw
        else:
            self.first_action = False
        ### end if

        self.last_state = obs_vec
        self.last_action = action

        if done:
            print("new action values: {}".format(np.dot(self.w, obs_vec)))
            self.agent_start()

        return action

    def q(self, state_vec, action):
        return np.dot(self.w, state_vec)[action]

    def dq(self, state_vec, action):
        retval = np.zeros(self.w.shape)
        retval[action, :] = state_vec.T
        return retval

    def reset_agent(self, seed=None):
        self.w = np.zeros((self.action_space.n, self.n_neurons + 1))
