import time
from random import Random


class RandomAgent(object):
    def __init__(self, action_space):
        self.info = {"class": type(self).__name__}

        self.rand_generator = Random()
        self.action_space = action_space

    def act(self, observation, reward, done):
        time.sleep(0.2)
        return self.action_space.sample()

    def reset_agent(self, seed=None):
        pass
