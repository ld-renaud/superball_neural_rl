import random
from copy import deepcopy

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from gym.core import Env
from gym.spaces import Box, Discrete


class EmptyGridEnvironment(Env):
    def __init__(
        self,
        grid_dimensions,
        target=[9, 9],
        init_state=[None, None],
        generic_reward=0,
        map_violation_reward=0,
    ):
        if map_violation_reward == 0 and generic_reward < map_violation_reward:
            map_violation_reward = generic_reward
        # Saving parameters in serializable dict
        ########################################
        self.info = deepcopy(locals())
        del self.info["self"]
        self.info["class"] = type(self).__name__
        ########################################

        self.init_state = init_state
        self.target = np.array(target)
        self.generic_reward = generic_reward
        self.map_violation_reward = map_violation_reward

        # Set up grids
        ######################
        if not (
            isinstance(grid_dimensions, (list, tuple))
            and len(grid_dimensions) == 2
            and all([isinstance(element, int) for element in grid_dimensions])
        ):
            raise ValueError(
                "grid dimensions are excepted to be tuple or list of 2 integers"
            )
        self.grid = np.ones(grid_dimensions) * 255

        # Set up target position
        ########################
        if not self._in_boundaries(target):
            raise ValueError("Target is out of in_boundaries")
        if self._on_target(init_state):
            raise ValueError("Robot already on target")

        # Action and state spaces description
        ######################################
        self._set_description()

        return

    def _set_description(self):
        self.action_space = Discrete(4)
        self.observation_space = Box(
            low=np.array([0, 0]), high=np.array(self.grid.shape) - np.array([1, 1])
        )

        self.metadata = {"render.modes": ["human"]}
        self.reward_range = (0, 1)

    def _reset_agent(self):
        if None in self.init_state:
            while True:
                self.agent_pos = np.array(
                    [
                        np.random.randint(0, self.grid.shape[1]),
                        np.random.randint(0, self.grid.shape[0]),
                    ]
                )
                if not self._on_target(self.agent_pos) and self._is_empty(
                    self.agent_pos
                ):
                    break
        else:
            self.agent_pos = np.array(self.init_state)

    def _reset_viz(self):
        self.fig = plt.figure("Grid path planing env")
        plt.clf()
        self.ax = self.fig.add_subplot(111)
        plt.ion()

        #  Draw map
        self.ax.imshow(self.grid, cmap="gray", vmin=0, vmax=255, origin="lower")
        self.ax.set_xlim(-0.5, self.grid.shape[0] - 0.5)
        self.ax.set_ylim(-0.5, self.grid.shape[1] - 0.5)

        # Draw target
        target_patch = matplotlib.patches.Rectangle(
            xy=(self.target[0] - 0.5, self.target[1] - 0.5),
            width=1,
            height=1,
            color="forestgreen",
        )
        self.ax.add_patch(target_patch)

        # Draw robot
        self.agent_patches = []

    def reset(self):

        self.iter = 0
        self.done = False

        self._reset_agent()

        # Graphic visualization
        #######################
        self._reset_viz()

        init_state = {"position": self.agent_pos}
        return init_state

    def close(self):
        pass

    def seed(self, seed=None):
        np.random.seed(seed)
        return seed

    def step(self, action):
        if action not in self.action_space:
            raise ValueError("Specified action is not in the action space")

        print("Action {} chosen".format(action))

        self.iter += 1
        reward = self.generic_reward

        # Computing next position of the agent
        ######################################
        if action == 0:  # right
            new_pos = self.agent_pos + np.array([1, 0])
        elif action == 1:  # up
            new_pos = self.agent_pos + np.array([0, 1])
        elif action == 2:  # down
            new_pos = self.agent_pos + np.array([-1, 0])
        elif action == 3:  # left
            new_pos = self.agent_pos + np.array([0, -1])

        in_boundaries = self._in_boundaries(new_pos)
        is_empty = self._is_empty(new_pos)
        if in_boundaries and is_empty:
            self.agent_pos = new_pos
        elif not in_boundaries:
            reward = self.map_violation_reward
            print("Next position out of map, staying in previous position")
        elif not is_empty:
            reward = self.map_violation_reward
            print("Next position in obstacle staying in previous position")

        if self._on_target(self.agent_pos):
            print("Agent is on target !")
            reward = 1
            self.done = True

        new_state = {"position": self.agent_pos}
        return (new_state, reward, self.done, {})

    def _in_boundaries(self, point):
        in_boundaries = (
            0 <= point[0]
            and point[0] < self.grid.shape[1]
            and 0 <= point[1]
            and point[1] < self.grid.shape[0]
        )
        return in_boundaries

    def _is_empty(self, point):
        return True

    def _on_target(self, point):
        return (point == self.target).all()

    def _update_plot(self):
        if self.agent_patches:
            for i, agent_patch in enumerate(reversed(self.agent_patches)):
                gray_level = min((100 + i * 10) / 255, 1)
                agent_patch.set_color((gray_level, gray_level, gray_level))

        # Draw agent
        agent_xy = (self.agent_pos[0] - 0.5, self.agent_pos[1] - 0.5)
        agent_patch = matplotlib.patches.Rectangle(
            xy=agent_xy, width=1, height=1, color="steelblue"
        )
        self.ax.add_patch(agent_patch)
        self.agent_patches.append(agent_patch)

        plt.draw()
        plt.pause(0.001)
        plt.show()
        return

    def render(self, mode="human"):
        if mode != "human":
            super(EmptyGridEnvironment, self).render(mode=mode)
        ### end if
        self._update_plot()
