import random
from copy import deepcopy
from pathlib import Path

import numpy as np

from PIL import Image

from .empty_grid_env import EmptyGridEnvironment


class RandomGridEnvironment(EmptyGridEnvironment):
    resources_path = (Path(__file__).parents[0] / "../../resources/grid_env/").resolve()

    def __init__(
        self,
        grid_dimensions=(5, 5),
        target=[2, 2],
        init_state=[None, None],
        generic_reward=0,
        map_violation_reward=0,
        obstacles_introduction=0,
    ):
        if map_violation_reward == 0 and generic_reward < map_violation_reward:
            map_violation_reward = generic_reward
        # Saving parameters in serializable dict
        ########################################
        self.info = deepcopy(locals())
        del self.info["self"]
        self.info["class"] = type(self).__name__
        ########################################

        self.reset_nb = 0
        self.init_state = init_state
        self.target = np.array(target)
        self.generic_reward = generic_reward
        self.map_violation_reward = map_violation_reward
        self.obstacles_introduction = obstacles_introduction

        # Set up grid
        ######################

        grids = [
            file
            for file in self.resources_path.iterdir()
            if file.suffix == ".png" and "{}x{}".format(*grid_dimensions) in file.name
        ]

        self.empty_grid = np.ones(grid_dimensions) * 255
        self.grids = [np.array(Image.open(Path(grid)).convert("L")) for grid in grids]
        for grid in self.grids:
            self.grid = grid
            if not self._is_empty(target):
                raise ValueError("Target is in an obstacle in one of the grids")
            if not self._in_boundaries(target):
                raise ValueError("Target is out of in_boundaries in one of the grids")

        # Set up target position
        ########################

        if self._on_target(init_state):
            raise ValueError("Robot already on target")

        # Action and state spaces description
        ######################################
        self._set_description()

        return

    def reset(self):
        self.reset_nb += 1
        self.iter = 0
        self.done = False

        self._reset_grid()
        self._reset_agent()

        # Graphic visualization
        #######################
        self._reset_viz()

        init_state = {"position": self.agent_pos, "grid": self.grid}
        return init_state

    def _reset_grid(self):
        if self.reset_nb >= self.obstacles_introduction:
            grid = random.choice(self.grids)
            self.grid = np.rot90(grid, np.random.randint(0, 4))
        else:
            self.grid = self.empty_grid

    def _is_empty(self, point):
        if (np.array(point[::-1]) < np.array(self.grid.shape)).all():
            is_empty = self.grid[tuple(point)[::-1]] == 255
        else:
            is_empty = False
        return is_empty

    def step(self, action):
        new_state, reward, self.done, _ = super().step(action)
        new_state["grid"] = self.grid
        return (new_state, reward, self.done, {})
