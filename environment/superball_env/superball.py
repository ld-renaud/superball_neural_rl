from enum import Enum

import numpy as np


class Triangle_type(Enum):
    CW = 1
    CCW = -1


class SUPERBall:
    string_len = 1
    diameter = 1

    def __init__(
        self, position=[1.0, 1.0], init_orientation=0, cbt_type=Triangle_type.CW
    ):
        self.position = np.array(position)
        self.yaw = init_orientation
        self.cbt_type = cbt_type  # current base triangle

    def get_next_pos(self, direction):
        next_pos = self.position.copy()

        if self.cbt_type == Triangle_type.CW:
            angle = self.yaw - np.pi / 6 + 2 * np.pi / 3 * (direction - 1)
        elif self.cbt_type == Triangle_type.CCW:
            angle = self.yaw + np.pi / 6 - 2 * np.pi / 3 * (direction - 1)
        else:
            raise ValueError("Unexpected triangle type")

        next_pos += np.array(
            [SUPERBall.string_len * np.sin(angle), SUPERBall.string_len * np.cos(angle)]
        )

        return next_pos

    def get_cbt_coords(self, position=None):
        if position is None:
            position = self.position
        x, y = position
        coords = []
        for angle in [self.yaw + i * 2 * np.pi / 3 for i in range(3)]:
            coords.append(
                np.array(
                    [
                        x + SUPERBall.string_len / 2 * np.sin(angle),
                        y + SUPERBall.string_len / 2 * np.cos(angle),
                    ]
                )
            )

        return np.array(coords)
