from copy import deepcopy
from pathlib import Path

import numpy as np

from .superball import SUPERBall, Triangle_type
from .superball_env import SuperballEnvironment


class RandomSuperballEnvironment(SuperballEnvironment):
    resolution = 0.01
    maps_folder = (
        Path(__file__).parents[0] / "../../resources/superball_env/"
    ).resolve()

    # Parameters to ensure that each randomly generated target is reachable
    #######################################################################

    margin = SUPERBall.string_len
    # (1 + 2 * np.cos(np.pi / 3)) * SUPERBall.string_len / 2 == SUPERBall.string_len

    padding = 1.75  # for SUPERBall.string_len = 1

    def __init__(self, size=750, generic_reward=0):
        # Saving parameters in serializable dict
        ########################################
        self.info = deepcopy(locals())
        del self.info["self"]
        del self.info["__class__"]
        self.info["class"] = type(self).__name__

        # Creating member variables
        ########################################
        self.generic_reward = generic_reward
        self.map_violation_reward = generic_reward

        # Set up map from an image
        ##############################
        if size not in [500, 750, 1000, 1250]:
            raise ValueError(
                "'size' parameter is expected to be one of [500, 750, 1000]"
            )
        map_path = self.maps_folder / "empty_map_{}p.png".format(size)
        super()._set_map(
            map_path, RandomSuperballEnvironment.resolution
        )

        # Set up robot and target position
        ##################################
        self._set_robot()
        self._set_target()

        # Action and state spaces description
        ######################################
        self._set_description()
        print(RandomSuperballEnvironment.margin)

    ### end __init__

    def _set_robot(self):
        # Creating initial state
        #########################
        self.init_state = {
            "position": self.get_random_pos(),
            "cbt_type": np.random.choice([Triangle_type.CW, Triangle_type.CW]),
        }

        # Set up robot
        ###############
        self.robot = SUPERBall(**self.init_state)

    def _set_target(self):
        # Set up target
        ###############
        self.target = self.get_random_pos()
        while self._on_target(self.robot.position):
            self.target = self.get_random_pos()
        self.init_state["target"] = self.target

    def reset(self):
        # reset variables, robot and target
        ####################################
        self.iter = 0
        self.done = False

        self._set_robot()
        self._set_target()

        # Graphic visualization
        #######################
        self._reset_viz()

        return self.init_state

    def step(self, action):
        new_state, reward, self.done, _ = super().step(action)
        new_state["target"] = self.target
        return (new_state, reward, self.done, {})

    def get_random_pos(self):
        position = np.random.random(2) * np.array(
            [
                self.xmax - RandomSuperballEnvironment.padding * 2,
                self.ymax - RandomSuperballEnvironment.padding * 2,
            ]
        ) + np.array([RandomSuperballEnvironment.padding] * 2)
        return position
