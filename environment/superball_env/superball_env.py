from copy import deepcopy
from pathlib import Path

import numpy as np

import matplotlib.pyplot as plt
from gym.core import Env
from gym.spaces import Box, Dict, Discrete
from PIL import Image

from .superball import SUPERBall, Triangle_type


class SuperballEnvironment(Env):
    def __init__(
        self,
        map_path,
        target,
        resolution=0.01,
        init_state={"position": [1.0, 1.0], "cbt_type": Triangle_type.CW},
        margin=0.75,
        generic_reward=0,
        map_violation_reward=0,
    ):
        # Saving parameters in serializable dict
        ########################################
        if map_violation_reward == 0 and generic_reward < map_violation_reward:
            map_violation_reward = generic_reward
        self.info = deepcopy(locals())
        del self.info["self"]
        self.info["class"] = type(self).__name__
        self.info["init_state"]["position"] = list(self.info["init_state"]["position"])
        self.info["init_state"]["cbt_type"] = self.info["init_state"]["cbt_type"].name

        # Creating member variables
        ########################################
        self.init_state = init_state
        self.margin = margin
        self.generic_reward = generic_reward
        self.map_violation_reward = map_violation_reward

        # Set up map from an image
        ##############################
        self._set_map(map_path, resolution)

        # Set up robot and target position
        ##################################
        self._set_target(target)
        self._set_robot(init_state)

        # Action and state spaces description
        ######################################
        self._set_description()

    ### end __init__

    def _set_map(self, map_path, resolution):
        map_path = Path(map_path)
        print("Loading map at: {}".format(map_path))

        img = Image.open(map_path).convert("L")
        self.map = np.array(img)
        self.res = resolution  # resolution * pixel = meter
        self.xmax = self.map.shape[0] * self.res
        self.ymax = self.map.shape[1] * self.res
        print(
            "Map size: {}m x {}m ({}p x {}p)".format(
                self.xmax, self.ymax, self.map.shape[0], self.map.shape[1]
            )
        )
        self.info["xmax"] = self.xmax
        self.info["ymax"] = self.ymax

    def _set_robot(self, init_state):
        self.robot = SUPERBall(**init_state)

        if self._on_target(init_state["position"]):
            raise ValueError("Robot already on target")

    def _set_target(self, target):
        self.target = target

        if self._in_obstacle(target):
            raise ValueError("Target is in an obstacle")
        elif not self._in_boundaries(target):
            raise ValueError("Target is out of in_boundaries")
        print("Target : {}m, {}m".format(target[0], target[1]))

    def _set_description(self):
        self.action_space = Discrete(3)
        self.observation_space = Dict(
            {
                "position": Box(
                    low=np.array([0.0, 0.0]), high=np.array(self.map.shape) * self.res
                ),
                "cbt_type": Discrete(2),
            }
        )

        self.metadata = {"render.modes": ["human"]}
        self.reward_range = (0, 1)

    def reset(self):
        # reset variables and robot
        #############################
        self.iter = 0
        self.done = False

        self._set_robot(self.init_state)

        # Graphic visualization
        #######################
        self._reset_viz()

        return self.init_state

    def _reset_viz(self):
        self.fig = plt.figure("SUPERBall path planing env")
        plt.clf()
        self.ax = self.fig.add_subplot(111)
        plt.ion()
        plt.show()

        #  Draw map
        self.ax.imshow(self.map, cmap="gray", vmin=0, vmax=255, origin="lower")

        # Draw target
        target_pixels = tuple([meter / self.res for meter in self.target])
        target_patch = plt.Circle(
            target_pixels, self.margin / self.res, color="forestgreen"
        )
        self.ax.add_patch(target_patch)

        # Draw robot
        self.robot_patches = []

    def step(self, action):
        if action not in self.action_space:
            raise ValueError("Specified action is not in the action space")

        print("Action {} chosen".format(action))

        self.iter += 1
        reward = self.generic_reward

        # Computing next position of the robot
        ######################################
        new_pos = self.robot.get_next_pos(action)

        # Checking for feasibitiy of the new position
        #############################################
        new_bt_coords = self.robot.get_cbt_coords(new_pos)
        map_violation = False

        # Check boudaries
        for point in new_bt_coords:
            if not self._in_boundaries(point):
                print("Next position out of map, staying in previous position")
                map_violation = True
                reward = self.map_violation_reward
                break
            ### end if
        ### end for

        # Check obstacle
        if not map_violation:
            map_violation = self._in_obstacle(
                point=new_pos, radius=self.robot.diameter / 2
            )
            if map_violation:
                print("Next position in obstacle, staying in previous position")
                reward = self.map_violation_reward
        ### end if not map_violation

        if not map_violation:
            # Updating robot state
            ######################
            self.robot.position = new_pos
            self.robot.cbt_type = (
                Triangle_type.CCW
                if self.robot.cbt_type == Triangle_type.CW
                else Triangle_type.CW
            )

        ### end if not map_violation

        # Reward computation
        ######################
        if self._on_target(self.robot.position):
            print("Robot is on target !")
            reward = 1
            self.done = True

        new_state = {"position": self.robot.position, "cbt_type": self.robot.cbt_type}
        return (new_state, reward, self.done, {})

    def render(self, mode="human"):
        if mode != "human":
            super(SuperballEnvironment, self).render(mode=mode)
        ### end if
        self._update_plot()

    def close(self):
        pass

    def seed(self, seed=None):
        np.random.seed(seed)
        return seed

    def _in_boundaries(self, point):
        in_boundaries = (
            0 < point[0]
            and point[0] < self.xmax
            and 0 < point[1]
            and point[1] < self.ymax
        )
        return in_boundaries

    def _in_obstacle(self, point, radius=None):
        b, a = [x / self.res for x in point]

        radius = self.res if radius is None else radius
        r = radius / self.res

        y, x = np.ogrid[-a : self.map.shape[0] - a, -b : self.map.shape[1] - b]
        mask = x * x + y * y <= r * r

        in_obstacle = np.any(self.map[mask] < 255)
        return in_obstacle

    def _on_target(self, point):
        on_target = (
            (point[0] - self.target[0]) ** 2 + (point[1] - self.target[1]) ** 2
        ) <= self.margin ** 2
        return on_target

    def _update_plot(self):
        if self.robot_patches:
            self.robot_patches[-1].set_fill(False)
            self.robot_patches[-1].set_color("silver")
            self.radius_patch.remove()

        # Draw robot
        robot_patch = plt.Polygon(
            self.robot.get_cbt_coords() / self.res, color="steelblue"
        )
        self.radius_patch = plt.Circle(
            (self.robot.position[0] / self.res, self.robot.position[1] / self.res),
            radius=self.robot.diameter / (self.res * 2),
            fill=False,
            color="gold",
            ls="--",
        )

        self.ax.add_patch(robot_patch)
        self.ax.add_patch(self.radius_patch)
        self.ax.text(
            self.robot.position[0] / self.res,
            self.robot.position[1] / self.res - 10,
            "{}".format("cw" if self.robot.cbt_type == Triangle_type.CW else "ccw"),
            ha="center",
        )

        self.robot_patches.append(robot_patch)

        plt.draw()
        plt.pause(0.001)
        plt.show()
        return


###

if __name__ == "__main__":
    e = SuperballEnvironment(
        "../../resources/superball_env/empty_map_1000p.png", target=(9, 9)
    )
