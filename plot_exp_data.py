#!/usr/bin/env python3
import argparse
import bisect
import json

from pathlib import Path

import numpy as np

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

import dijkstra

# Command line parser
######################
parser = argparse.ArgumentParser(
    description="Creates plots of data from an experiment."
)
parser.add_argument(
    "path", type=str, help="path to a directory or a json file to parse"
)
parser.add_argument(
    "-np", "--no_probes", action="store_true", help="Skip plots of probes"
)

parser.add_argument(
    "-e", "--episodes", nargs="+", type=int, help="List of episodes to plot"
)

# Constants
############

# Parameters of background coloring representing phases
RESET_BKG_COLOR = "peachpuff"
LEARNING_BKG_COLOR = "paleturquoise"
DONE_BKG_COLOR = "mistyrose"

BKG_LEGEND_HANDLES = [
    mpatches.Patch(color=RESET_BKG_COLOR, label="reset phases"),
    mpatches.Patch(color=LEARNING_BKG_COLOR, label="learning phases"),
    mpatches.Patch(color=DONE_BKG_COLOR, label="first/last phases"),
]

BKG_LEGEND_LABELS = ["reset phases", "learning phases", "first/last phases"]

# Setting up adequate pyplot parameters

SMALL_SIZE = 5
MEDIUM_SIZE = 7
BIGGER_SIZE = 8
DPI = 250

plt.rc("font", size=SMALL_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=MEDIUM_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc("legend", fontsize=SMALL_SIZE)  # legend fontsize
plt.rc("figure", titlesize=BIGGER_SIZE)  # fontsize of the figure title

plt.rc("lines", linewidth=1)
plt.rc("lines", markersize=3)


#  Main function
#################
def plot_exp_data(file_path, plot_probes=True, episodes_to_plot=[]):

    print("\nPlotting experiment data")
    print("\n__________________________________________\n")
    print("file path : {}".format(file_path))
    print("plot probes: {}".format(plot_probes))
    print("")

    file_path = Path(file_path)
    file_dir = file_path.parents[0]
    data = load(file_path)

    plots_dir = file_dir / file_path.stem
    plots_dir.mkdir(exist_ok=True)

    # Processing for each episode
    ##################################
    performances = []
    performances_min = []
    action_sequences = []
    state_sequences = []
    episode_start_time = 0

    if data["agent"]["class"] == "NeuralAgent":
        learning_duration = data["agent"]["learning_duration"]
        reset_duration = data["agent"]["reset_duration"]
        selection_duration = data["agent"]["selection_duration"]
        delay_duration = data["agent"]["delay_duration"]

        done_duration = learning_duration + delay_duration

    for episode_data in data["episodes"]:
        # Selection corresponding environment
        #########################################
        environment_data = select_environment(data, episode_data["number"])

        # Saving sequences of state and action taken
        ############################################
        action_sequences.append(episode_data["actions"])
        state_sequences.append(episode_data["states"])

        # Computing performance metric
        ##################################
        performance, performance_min = compute_performance(data, episode_data["number"])
        performances.append(performance)
        performances_min.append(performance_min)

        if plot_probes and data["agent"]["class"] == "NeuralAgent":
            plot_episode = episodes_to_plot and (
                episode_data["number"] in episodes_to_plot
            )

            if plot_episode:
                print("episode_{} ...".format(episode_data["number"]))
            # Creating the folder for plots current episode
            ###############################################
            episode_dir = plots_dir / "episode_{}".format(episode_data["number"])
            if not episode_dir.is_dir():
                episode_dir.mkdir()

            # Actual plots of probes
            ############################
            trange = episode_data["trange"]

            # Computing figure size
            fig_x = min(max(1, (trange[-1] - trange[0]) * 1 / 3), 6.5)

            fig = plt.figure(figsize=(fig_x, 3))
            ax = fig.add_subplot(111)

            for label, probe_data in episode_data["probes"].items():
                # t0 = time.time()
                dir_name, probe_name = label.split(".")
                dir = episode_dir / dir_name
                if not dir.is_dir():
                    dir.mkdir()

                probe_data = np.array(probe_data)
                probe_lines = ax.plot(trange, probe_data)

                if plot_episode:
                    # Formating
                    ax.grid(axis="y", linestyle="-")
                    ax.set_title(
                        "episode {}: {}".format(episode_data["number"], probe_name)
                    )
                    ax.set_xlabel("time [s]")
                    ax.set_ylabel("decoded value")
                    # y_max = np.amax(probe_data)
                    # y_min = np.amin(probe_data)
                    # ax.set_ylim(bottom=np.floor(y_min) * 1.1, top=np.ceil(y_max) * 1.1)
                    ax.set_ylim(bottom=-1.25, top=1.25)

                    if any(
                        [
                            name == probe_name
                            for name in ["current_Q", "prev_Q", "error_vector"]
                        ]
                    ):
                        probe_lines_labels = [
                            "action {}".format(i) for i in range(len(probe_lines))
                        ]
                    elif any([name == probe_name for name in ["state", "prev_state"]]):
                        probe_lines_labels = ["x", "y", "cbt"][: len(probe_lines)]
                    else:
                        probe_lines_labels = [
                            "dim {}".format(i) for i in range(len(probe_lines))
                        ]

                    ax.legend(
                        handles=probe_lines + BKG_LEGEND_HANDLES,
                        labels=probe_lines_labels + BKG_LEGEND_LABELS,
                        loc="upper center",
                        bbox_to_anchor=(0.5, -0.25),
                        ncol=4,
                    )

                # Coloring background depending on phases
                t = episode_start_time

                # first action selection step
                ax.axvspan(t, t + delay_duration, color=DONE_BKG_COLOR, alpha=0.5)
                t += delay_duration
                ax.axvspan(t, t + reset_duration, color=RESET_BKG_COLOR, alpha=0.5)
                t += reset_duration

                # following action selection steps
                while (
                    t + delay_duration + learning_duration + selection_duration
                    < trange[-1] - done_duration
                ):
                    # delay phase
                    t += delay_duration
                    # learning phase
                    ax.axvspan(
                        t, t + learning_duration, color=LEARNING_BKG_COLOR, alpha=0.5
                    )
                    t += learning_duration
                    # selection phase
                    t += selection_duration
                    # reset/saving phases
                    ax.axvspan(t, t + reset_duration, color=RESET_BKG_COLOR, alpha=0.5)
                    t += reset_duration

                # final step after an episode is done
                ax.axvspan(
                    trange[-1] - done_duration,
                    trange[-1],
                    color=DONE_BKG_COLOR,
                    alpha=0.5,
                )
                t += done_duration

                # t1 = time.time()
                if plot_episode:
                    plt.savefig(
                        dir / "{}.png".format(probe_name),
                        dpi=DPI,
                        bbox_inches="tight",
                        pad_inches=0.2,
                    )
                # t2 = time.time()
                ax.clear()
                # t3 = time.time()
                # print("total time: {:5g} s".format(t3 - t0))
                # print("savefig time: {:.5g} s".format(t2 - t1))
            plt.close()
            episode_start_time = t

    # Plot the performance evolution over episodes
    ##############################################
    # Creating figure
    nb_episodes = len(data["episodes"])
    print("Plot performance evolution ...")

    fig_x = min(max(3, nb_episodes * 6.5 / 100), 6.5)
    fig = plt.figure(figsize=(fig_x, 3.25), dpi=DPI)
    ax = fig.add_subplot(111)

    y_label = (
        "regret" if "GridEnvironment" in environment_data["class"] else "step number"
    )

    # Plotting data
    if performances_min:
        ax.step(
            np.arange(1, nb_episodes + 1),
            performances_min,
            color="r",
            linestyle="--",
            where="mid",
            label="{} limit".format(y_label),
        )
    ax.plot(np.arange(1, nb_episodes + 1), performances, marker=".", label=y_label)

    # Formatting figure
    ax.set_title("Performance evolution")
    ax.grid(linestyle="--")
    ax.legend()
    ax.set_xlabel("episode")
    ax.set_ylabel(y_label)
    plt.xticks(np.arange(0, nb_episodes + 1, 2 * max(1, nb_episodes // 50)))
    plt.yticks(
        np.arange(0, int(1.1 * max(performances)), max(1, max(performances) // 9))
    )
    ax.set_xlim(0, nb_episodes + 1)

    plt.savefig(
        plots_dir / "performance.png", bbox_inches="tight", dpi=DPI, pad_inches=0.2
    )
    fig.clear()
    plt.close()

    # Description of the parameters of the experiment
    #################################################
    description_file = plots_dir / "parameters.txt"
    print("writing description of experiment ...")
    with open(description_file, "w") as f:
        title = "Experiment {}: \n_____________________________\n\n"
        f.write(title.format(file_path.name.strip(".json")))

        f.write("Number of episode: {}\n".format(len(data["episodes"])))
        for key, value in data.items():
            if key not in ["environments", "agent", "episodes"]:
                f.write("{} : {}\n".format(key, str(value)))

        f.write("\n\nEnvironments:\n--------------\n")
        for start_episode, environment_data in data["environments"].items():
            f.write("\nfrom episode {}:\n".format(start_episode))
            for key, value in environment_data.items():
                f.write("{} : {}\n".format(key, str(value)))

        f.write("\nAgent:\n--------------\n")
        for key, value in data["agent"].items():
            f.write("{} : {}\n".format(key, str(value)))

        f.write("\nFrequent paths:\n--------------\n")
        for action_sequence in unique(action_sequences):
            count = action_sequences.count(action_sequence)
            if count > 2:
                f.write(
                    "\n{}\nOccured {} times\nSteps number: {}\n".format(
                        action_sequence, count, len(action_sequence)
                    )
                )

        to_write = ["steps", "real_duration", "sim_duration"]
        for i, episode_data in enumerate(data["episodes"]):
            f.write(
                "\nEpisode {:.3g}:\n--------------\n".format(episode_data["number"])
            )
            for key, value in episode_data.items():
                if key in to_write:
                    f.write("{} : {}\n".format(key, str(value)))

    print("Done")


def select_environment(data, episode):
    if "environments" in data:
        keys = sorted([int(key) for key in data["environments"].keys()])
        i = bisect.bisect(keys, episode)
        environment_data = data["environments"][str(keys[i - 1])]
    elif "environment" in data:
        environment_data = data["environment"]
    else:
        raise ValueError("No environment(s) key in data dict")
    return environment_data


def compute_performance(data, episode):
    environment_data = select_environment(data, episode)
    episode_data = data["episodes"][episode - 1]

    performance_min = None

    # Computing performance metric
    ##################################

    # SUPERball
    if environment_data["class"] == "SuperballEnvironment":
        performance = episode_data["steps"]
        if data["max_steps"] is not None:
            performance_min = data["max_steps"]
    # Random SUPERball
    elif environment_data["class"] == "RandomSuperballEnvironment":
        performance = episode_data["steps"] / np.linalg.norm(
            np.multiply(
                episode_data["states"][0][:2],
                [environment_data["xmax"], environment_data["ymax"]],
            )
        )
        if data["max_steps"] is not None:
            performance_min = data["max_steps"]

    # Empty Gridworld
    elif any(
        [
            environment_data["class"] == env
            for env in ["EmptyGridEnvironment", "GridEnvironment"]
        ]
    ):
        init_pos = (
            (np.array(episode_data["states"][0][:2]) + np.ones(2))
            * np.array(environment_data["grid_dimensions"] - np.ones(2))
            / 2
        )
        minimum_step = int(
            np.sum(np.absolute(init_pos - np.array(environment_data["target"])))
        )
        performance = episode_data["steps"] - minimum_step

        if data["max_steps"] is not None:
            performance_min = data["max_steps"] - minimum_step

    # Random Gridworld
    elif environment_data["class"] == "RandomGridEnvironment":
        init_pos = (
            (np.array(episode_data["states"][0][:2]) + np.ones(2))
            * (np.array(environment_data["grid_dimensions"]) - np.ones(2))
            / 2
        )
        target = environment_data["target"]
        grid = episode_data["grid"]

        path = dijkstra.get_min_path(grid, init_pos, target)
        minimum_step = len(path) - 1

        performance = episode_data["steps"] - minimum_step
        if data["max_steps"] is not None:
            performance_min = data["max_steps"] - minimum_step

    return performance, performance_min


def load(file_path):
    file_path = Path(file_path)
    if file_path.is_file():
        with open(file_path, "r") as f:
            data = json.load(f)
            print("Data loaded successfully")
            return data
    else:
        raise ValueError("Specified file does not exist")


def unique(list):
    # intilize a null list
    unique_list = []

    # traverse for all elements
    for x in list:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)
    return unique_list


def average_performance(dir):
    print("\nPlotting average performance ...")

    dir = Path(dir)
    if not dir.is_dir():
        raise ValueError("Specified directory does not exist")

    data_dicts = list()
    json_files = [x for x in dir.iterdir() if x.suffix == ".json"]

    if len(json_files) <= 1:
        print("Only one or less iteration found, no average perforance plot")
        return

    for file in json_files:
        data_dicts.append(load(file))

    all_perfomances = list()
    for data in data_dicts:
        performances = list()
        for episode_data in data["episodes"]:
            # Selection corresponding environment
            #########################################
            environment_data = select_environment(data, episode_data["number"])

            performance, performance_min = compute_performance(
                data, episode_data["number"]
            )
            performances.append(performance)

        all_perfomances.append(performances)

    max_episode_nb = max([len(performance) for performance in all_perfomances])
    for performance in all_perfomances:
        episode_nb_diff = max_episode_nb - len(performance)
        performance.extend([np.nan] * episode_nb_diff)

    performances_mean = np.nanmean(np.asarray(all_perfomances), axis=0)
    performances_std = np.nanstd(np.asarray(all_perfomances), axis=0)
    if data["max_steps"] is not None:
        performances_min = [data["max_steps"]] * max_episode_nb
    else:
        performances_min = []

    # Plot the performance evolution over episodes
    ##############################################
    # Creation of figure
    nb_episodes = max_episode_nb
    fig_x = min(max(3, nb_episodes * 6.5 / 100), 6.5)
    fig = plt.figure(figsize=(fig_x, 3.25), dpi=DPI)
    ax = fig.add_subplot(111)

    y_label = (
        "mean regret"
        if "GridEnvironment" in environment_data["class"]
        else "mean step number"
    )

    # Plotting data
    if performances_min:
        ax.step(
            np.arange(1, nb_episodes + 1),
            performances_min,
            color="red",
            linestyle="--",
            where="mid",
            label="limit",
        )
    ax.plot(
        np.arange(1, nb_episodes + 1),
        performances_mean,
        marker=".",
        label=y_label,
        color="blue",
    )
    upper_std = np.array(performances_mean) + np.array(performances_std)
    lower_std = np.array(performances_mean) - np.array(performances_std)
    ax.fill_between(
        np.arange(1, nb_episodes + 1), lower_std, upper_std, color="lightblue"
    )

    # Formatting figure
    ax.set_title("Average performance evolution")
    ax.grid(linestyle="--")
    ax.legend()
    ax.set_xlabel("episode")
    ax.set_ylabel(y_label)
    plt.xticks(np.arange(0, nb_episodes + 1, 2 * max(1, nb_episodes // 50)))
    plt.yticks(np.arange(0, int(1.1 * max(upper_std)), max(1, max(upper_std) // 9)))

    ax.set_xlim(0, nb_episodes + 1)

    plt.savefig(dir / "average_performance.png", bbox_inches="tight", dpi=DPI)
    plt.close()
    print("Done")

    return


if __name__ == "__main__":
    args = parser.parse_args()

    path = Path(args.path).resolve()
    plot_probes = not args.no_probes
    episodes = args.episodes

    if not path.is_dir() and not path.is_file():
        raise ValueError("Specified path is neither a directory nor a file")
    elif path.is_dir():
        json_files = [x for x in path.iterdir() if x.suffix == ".json"]
        for json_file in json_files:
            if not (json_file.parents[0] / json_file.stem).is_dir():
                plot_exp_data(json_file, plot_probes, episodes)
            else:
                print("Skipping {}".format(json_file.name))
        average_performance(path)
    elif path.is_file():
        plot_exp_data(path, plot_probes, episodes)
