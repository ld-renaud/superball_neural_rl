import nengo
from agent import NeuralAgent, RandomAgent, TDAgent, ManualAgent
from environment import EmptyGridEnvironment
from run_experiment import run_experiment

# Parameters of the expertiment
###############################

EPISODES = 100
MAX_STEPS = None
RENDER = False
SEED = None
ITERATIONS = 10

GRID_DIM = (5, 5)
TARGET = (2, 2)
INIT_STATE = (None, None)

print("\n\nNew Experiment: Grid environment")
print("\n__________________________________________\n")

# Instantiate environment
#########################

print("\nInstantiating environment:\n--------------------------\n")
ENV_PARAM = {
    "grid_dimensions": GRID_DIM,
    "target": TARGET,
    "init_state": INIT_STATE,
    "generic_reward": 0,
}
env = EmptyGridEnvironment(**ENV_PARAM)
x_max, y_max = env.observation_space.high

# Instantiate agent
###################
print("\nInstantiating agent:\n--------------------------\n")

# agent = RandomAgent(env.action_space)
# agent = ManualAgent(env.action_space)

# agent = TDAgent(
#     env.action_space,
#     input_dim=3,
#     n_neurons=200,
#     discount=0.99,
#     learning_rate=1e-2,
#     epsilon=0.02,
#     sigma=0.15
# )

AGENT_PARAM = {
    "state_dim": 2,
    "action_dim": env.action_space.n,
    "n_neurons": 300,
    "neuron_type": nengo.LIF(),
    "learning_rate": 2e-4,
    "discount": 0.9,
    "noiselevel": 0.1,
    "Q_radius": 1,
    "reward_radius": 1,
    "state_dist_thresh": 0.25,
    "selection_duration": 0,
    "reset_duration": 0.3,
    "grbf_encoders": True,
    "seed": SEED,
    "backend": "nengo_dl",
    "gpu": None,
}
agent = NeuralAgent(**AGENT_PARAM)


def ob_to_state(ob):
    return [2 * (ob["position"][0] / x_max) - 1, 2 * (ob["position"][1] / y_max) - 1]


run_experiment(
    env=env,
    agent=agent,
    ob_to_state=ob_to_state,
    episodes=EPISODES,
    max_steps=MAX_STEPS,
    seed=SEED,
    render=RENDER,
    iterations=ITERATIONS,
)
