import nengo
from agent import NeuralAgent, RandomAgent, TDAgent, ManualAgent
from environment import SuperballEnvironment, Triangle_type
from run_experiment import run_experiment

# Parameters of the expertiment
###############################

EPISODES = 100
MAX_STEPS = None
RENDER = False
SEED = None
ITERATIONS = 10

TARGET = (4, 4)
MAP_PATH = "./resources/superball_env/empty_map_500p.png"

print("\n\nNew Experiment: SUPERBall environment")
print("\n__________________________________________\n")

# Instantiate environment
#########################
print("\nInstantiating environment:\n--------------------------\n")
ENV_PARAM = {
    "map_path": MAP_PATH,
    "target": TARGET,
    "resolution": 0.01,
    "margin": 0.75,
    "init_state": {"position": [1.0, 1.0], "cbt_type": Triangle_type.CW},
    "generic_reward": 0,
}

env = SuperballEnvironment(**ENV_PARAM)
x_max, y_max = env.observation_space["position"].high

# Instantiate agent
###################
print("\nInstantiating agent:\n--------------------------\n")

# agent = RandomAgent(env.action_space)
# agent = ManualAgent(env.action_space)

# agent = TDAgent(
#     env.action_space,
#     input_dim=3,
#     n_neurons=200,
#     discount=1,
#     learning_rate=5e-3,
#     epsilon=0.01,
#     sigma=0.2
# )

AGENT_PARAM = {
    "state_dim": 3,
    "action_dim": env.action_space.n,
    "n_neurons": 300,
    "neuron_type": nengo.LIF(),
    "learning_rate": 2e-4,
    "discount": 0.9,
    "noiselevel": 0.1,
    "Q_radius": 1,
    "reward_radius": 1,
    "state_dist_thresh": 0.35,
    "selection_duration": 0,
    "reset_duration": 0.3,
    "grbf_encoders": True,
    "grbf_sigma": 0.1,
    "seed": SEED,
    "backend": "nengo_dl",
    "gpu": None,
}
agent = NeuralAgent(**AGENT_PARAM)


def ob_to_state(ob):
    return [
        2 * ob["position"][0] / x_max - 1,
        2 * ob["position"][1] / y_max - 1,
        ob["cbt_type"].value,
    ]


run_experiment(
    env=env,
    agent=agent,
    ob_to_state=ob_to_state,
    episodes=EPISODES,
    max_steps=MAX_STEPS,
    seed=SEED,
    render=RENDER,
    iterations=ITERATIONS,
)
