# A neuromorphic path planner for SUPERBall based on reinforcement learning

## Abstract

Neuromorphic engineering is a brain-inspired way to carry out computations relying on networks of spiking neurons. It is a growing field which appears to offer advantages particularly relevant in the context of space research, such as low-power and noise-robust computations. We present an implementation of a neuromorphic reinforcement learning agent able to learn path planning for the tensegrity rover SUPERball. We show that our agent can learn online how to move between two arbitrary points on a map. In doing so, we assess the potential of several data encoding methods for spiking neural networks. First, we show that an encoding method based on Gaussian radial basis functions bring a large improvement to our agent’s behavior. Second, we explore how image encoding techniques like Gabor filters can be used to meaningfully transmit obstacle related information to our agent.

## Repository structure

- `agent/`: code for the agent and the nengo models
  - `neural_rl/`: package containing the architecture of the SNN (in nengo) used the neural agent
  - `neural_agent.py`: agent using a SNN
- `environment/`: code for the environments (based on gym)
  - `grid_env/`: agent have to reach a target in a grid world
  - `superball_env/`: agent have to reach a target moving in steps similars to SUPERball's
- `resources/`:
  - miscellaneous resources for the programs such a maps, etc...
- `experiment_*.py`: launch experiment with settings defined in the file via run_experiment
- `dijkstra.py`: Dijkstra's shortest path algorithm, compatible with gridworlds
- `plot_exp_data.py`: creates the plots and description file corresponding to the specified experiment data (saved in a json file)
- `run_experiment.py`: run a series of episodes with the specified agent and environment, save the data produced in a json file in experiments/
- `env.yml`: package requirements (use: conda env create -f env.yml)
