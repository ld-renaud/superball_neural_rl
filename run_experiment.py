import datetime
import json
import signal
import sys
import time
from pathlib import Path

from agent import NeuralAgent, RandomAgent, TDAgent
from environment import RandomGridEnvironment

# Global variables
exp_data = dict()
iteration = 0
folder_name = datetime.datetime.now().strftime("%b%d_%H%M%S")


def run_experiment(
    env,
    agent,
    ob_to_state,
    episodes=30,
    max_steps=None,
    seed=None,
    render=True,
    iterations=1,
):
    global iteration

    if type(env) == dict:
        env_list = env
        if 1 not in env_list.keys():
            raise ValueError("No environment specified for episode 1")
        env = env_list[1]
    else:
        env_list = {1: env}

    # Parameters of the expertiment
    ###############################
    exp_data["max_steps"] = max_steps
    exp_data["seed"] = seed

    # Environment
    ##############
    environments_info = dict()
    for start_episode in env_list:
        environments_info[start_episode] = env_list[start_episode].info

    exp_data["environments"] = environments_info

    # Agent
    #############
    exp_data["agent"] = agent.info

    # Experiment
    ############
    while iteration < iterations:
        iteration += 1

        env.seed(seed + iteration if seed is not None else None)
        agent.reset_agent(seed + iteration if seed is not None else None)

        print("\nIteration {}:\n-------------\n-------------\n".format(iteration))

        exp_data["episodes"] = list()
        episode = 1
        while episode <= episodes:
            # Selecting environment to use
            if episode in env_list.keys():
                env = env_list[episode]

            # Initialization of variables and infinite loop
            ob = env.reset()
            reward = 0
            done = False

            # Visualization
            if render:
                env.render()

            # State preprocessing
            state = ob_to_state(ob)

            # Data logging dicts
            episode_data = dict()
            episode_data["states"] = [state]
            episode_data["actions"] = list()
            episode_data["rewards"] = list()
            if isinstance(env, RandomGridEnvironment):
                episode_data["grid"] = env.grid.tolist()

            t_start = time.time()

            if isinstance(agent, NeuralAgent):
                t_start_sim = agent.sim.time

            print("\nEpisode {}:\n----------\n".format(episode))

            while True:
                # Select action
                print(
                    "\n[It{} - Ep{}] Step {}: Selecting action ...".format(
                        iteration, episode, env.iter
                    )
                )
                action = agent.act(state, reward, done)
                episode_data["actions"].append(action)

                ob, reward, done, _ = env.step(action)
                # Format and normalize observations for the agent
                state = ob_to_state(ob)

                episode_data["states"].append(state)
                episode_data["rewards"].append(reward)
                # print("new state: {}".format(state))
                print("reward: {}".format(reward))

                # Visualization
                if render:
                    env.render()

                if max_steps is not None:
                    if env.iter >= max_steps:
                        done = True

                if done:
                    agent.act(state, reward, done)

                    #  Saving data of episode
                    print("Saving data ...")
                    episode_data["number"] = episode
                    episode_data["steps"] = env.iter
                    episode_data["real_duration"] = time.time() - t_start

                    if isinstance(agent, NeuralAgent):
                        episode_data["sim_duration"] = agent.sim.time - t_start_sim
                        trange, probe_data = agent.get_probe_data()
                        episode_data["trange"] = trange
                        episode_data["probes"] = probe_data
                    exp_data["episodes"].append(episode_data)
                    episode += 1
                    break

        env.close()

        # Save data from the experiment to a json file
        ##################################################
        print("exp_data:\n{}".format(exp_data.keys()))
        save_data(exp_data)

        print("Iteration {}: Done".format(iteration))

    print("\n__________________________________________\n")
    # if iteration > 1:
    #     average_performance(Path(file_path).parents[1])
    print("\rExperiment Done")
    # return file_path


def save_data(data):
    print("\n--------------------------\nCreating experiment data file ...")
    location = (
        Path("./experiments") / data["environments"][1]["class"].lower() / folder_name
    )
    # Creation of directory if it does not exists
    location.mkdir(parents=True, exist_ok=True)

    # name of folder is the date and current time
    name = "{}_iter{}.json".format(folder_name, iteration)

    exp_file = location / name
    if exp_file.is_file():
        raise ValueError("Cannot create data file: {}\none already exists".format(name))
    else:
        with open(exp_file, "w") as f:
            json.dump(data, f, indent=2)

        return exp_file.as_posix()


# Handler for crlt+c signal support: save data before leaving program
def crtl_c_handler(sig, frame):
    print("\nKeyboard interpution: saving data of completed epidoses ...")
    if "episodes" in exp_data:
        if exp_data["episodes"]:
            save_data(exp_data)
        else:
            print("No data to save")
    else:
        print("No data to save")
    print("Exiting")
    sys.exit(0)


signal.signal(signal.SIGINT, crtl_c_handler)
