import numpy as np

import nengo
from agent import ManualAgent, NeuralAgent, RandomAgent, TDAgent
from environment import RandomGridEnvironment
from run_experiment import run_experiment

# Parameters of the expertiment
###############################

EPISODES = 100
MAX_STEPS = None
RENDER = False
SEED = None
ITERATIONS = 10

GRID_DIM = (5, 5)
IMG_DIM = None

print("\n\nNew Experiment: Grid environment")
print("\n__________________________________________\n")

# Instantiate environment
#########################

print("\nInstantiating environment:\n--------------------------\n")

env = RandomGridEnvironment(
    grid_dimensions=GRID_DIM, obstacles_introduction=0, map_violation_reward=-0.1
)
x_max, y_max = env.observation_space.high

# Instantiate agent
###################
print("\nInstantiating agent:\n--------------------------\n")

# agent = RandomAgent(env.action_space)
# agent = ManualAgent(env.action_space)

# agent = TDAgent(
#     env.action_space,
#     input_dim=2,
#     n_neurons=100,
#     discount=0.9,
#     learning_rate=1e-3,
#     epsilon=0.01,
# )

AGENT_PARAM = {
    "state_dim": 2 + IMG_DIM[0] * IMG_DIM[1] if IMG_DIM is not None else 2,
    "action_dim": env.action_space.n,
    "n_neurons": 300,
    "neuron_type": nengo.LIF(),
    "learning_rate": 5e-4,
    "discount": 0.9,
    "noiselevel": 0.1,
    "Q_radius": 1,
    "reward_radius": 1,
    "state_dist_thresh": 0.25,
    "selection_duration": 0,
    "reset_duration": 0.4,
    "grbf_encoders": True,
    "grbf_sigma": 0.15,
    "grbf_n_neurons": 200,
    "seed": SEED,
    "backend": "nengo_dl",
    "img_dim": IMG_DIM,
    "gabor_filters_dim": (2, 2),
    "gabor_n_neurons": 100,
    "gpu": None,
}
agent = NeuralAgent(**AGENT_PARAM)


# fig, ax = plt.subplots()


def ob_to_state(ob):
    state = [2 * (ob["position"][0] / x_max) - 1, 2 * (ob["position"][1] / y_max) - 1]

    if IMG_DIM is not None:
        # Whole grid
        # img = np.array(ob["grid"]) / 255

        # Grid centered on agent
        padding = int(np.floor(max(IMG_DIM) / 2))
        padded_grid = np.pad(ob["grid"], padding, mode="constant") / 255

        img_top = ob["position"][1] + padding + int(np.floor(IMG_DIM[0] / 2))
        img_bottom = ob["position"][1] + padding - int(np.floor(IMG_DIM[0] / 2))

        img_right = ob["position"][0] + padding + int(np.floor(IMG_DIM[1] / 2))
        img_left = ob["position"][0] + padding - int(np.floor(IMG_DIM[1] / 2))

        img = padded_grid[img_bottom : img_top + 1, img_left : img_right + 1]
        img = np.ones_like(img) - np.array(img)

        state.extend(img.flatten().tolist())
    return state


run_experiment(
    env=env,
    agent=agent,
    ob_to_state=ob_to_state,
    episodes=EPISODES,
    max_steps=MAX_STEPS,
    seed=SEED,
    render=RENDER,
    iterations=ITERATIONS,
)
