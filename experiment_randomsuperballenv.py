import nengo
from agent import NeuralAgent, RandomAgent, TDAgent, ManualAgent
from environment import RandomSuperballEnvironment
from run_experiment import run_experiment

# Parameters of the expertiment
###############################

EPISODES = 100
MAX_STEPS = None
RENDER = False
SEED = None
ITERATIONS = 10

print("\n\nNew Experiment: SUPERBall environment")
print("\n__________________________________________\n")

# Instantiate environment
#########################
print("\nInstantiating environment:\n--------------------------\n")

env = RandomSuperballEnvironment(size=750, generic_reward=0)
x_max, y_max = env.observation_space["position"].high

# Instantiate agent
###################
print("\nInstantiating agent:\n--------------------------\n")

# agent = RandomAgent(env.action_space)
# agent = ManualAgent(env.action_spac)

# agent = TDAgent(
#     env.action_space,
#     input_dim=3,
#     n_neurons=200,
#     discount=0.99,
#     learning_rate=1e-2,
#     epsilon=0.02,
#     sigma=0.15
# )

AGENT_PARAM = {
    "state_dim": 3,
    "action_dim": env.action_space.n,
    "n_neurons": 300,
    "neuron_type": nengo.LIF(),
    "learning_rate": 2e-4,
    "discount": 0.9,
    "noiselevel": 0.1,
    "Q_radius": 1,
    "reward_radius": 1,
    "state_dist_thresh": 0.35,
    "selection_duration": 0,
    "reset_duration": 0.3,
    "grbf_encoders": True,
    "grbf_sigma": 0.15,
    "seed": SEED,
    "backend": "nengo_dl",
    "gpu": None,
}
agent = NeuralAgent(**AGENT_PARAM)


def ob_to_state(ob):
    return [
        (ob["position"][0] - ob["target"][0]) / x_max,
        (ob["position"][1] - ob["target"][1]) / y_max,
        ob["cbt_type"].value,
    ]


run_experiment(
    env=env,
    agent=agent,
    ob_to_state=ob_to_state,
    episodes=EPISODES,
    max_steps=MAX_STEPS,
    seed=SEED,
    render=RENDER,
    iterations=ITERATIONS,
)
